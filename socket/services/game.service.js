import * as config from "../config";
import text from "../../data";
import { filterRooms } from "../helpers";

export default class Game {
    constructor(rooms, io, socket) {
        this.rooms = rooms;
        this.io = io;
        this.socket = socket;
    }

    get username() {
        return this.socket.handshake.query.username;
    }

    readyGame({ roomId }) {
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {

            if (user.username === this.username) {
                user.ready = !user.ready;
                user.ready ? room.usersReady++ : room.usersReady--;
            }
        });

        if (room.users.length === room.usersReady) {
            let timer = config.SECONDS_TIMER_BEFORE_START_GAME;

            const interval = setInterval(() => {
                timer--;
                this.io.to(roomId).emit("START_TIMER", timer);

                if (timer === 0) {
                    clearInterval(interval);

                    const payload = {
                        roomId,
                        counter: config.SECONDS_FOR_GAME,
                        id: Math.floor(Math.random() * text.texts.length)
                    };

                    this.io.to(roomId).emit("START_GAME", payload);

                    let gameCounter = config.SECONDS_FOR_GAME;

                    const counterInterval = setInterval(() => {
                        const room = this.rooms.get(roomId);

                        if (room.users.length === room.winners.length) {
                            clearInterval(counterInterval);
                        }

                        if (gameCounter === 0) {
                            clearInterval(counterInterval);

                            const winners = room.users.sort((a, b) => b.progress - a.progress);

                            winners?.forEach(user => {
                                room.winners = [...room.winners, user.username];
                            });

                            this.io.to(roomId).emit("END_GAME", { winners: room.winners, roomId });
                        }

                        gameCounter--;
                        this.io.to(roomId).emit("UPDATE_COUNTER", gameCounter);
                    }, 1000);
                }
            }, 1000);
        }

        this.rooms.set(roomId, room);
        this.io.to(roomId).emit("UPDATE_ROOM", room);
        this.socket.broadcast.emit("UPDATE_ROOMS", filterRooms(this.rooms));
    };


    updateProgress({ progress, roomId }) {
        this.io.to(roomId).emit("UPDATE_PROGRESS", { username: this.username, progress });
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {
            if (user.username === this.username) {
                user.progress = progress;
            }
        });

        this.rooms.set(roomId, room);

        if (progress === 100) {
            room.winners = [...room.winners, this.username]
            this.rooms.set(roomId, room);

            if (room.winners.length === room.users.length) {
                this.io.to(roomId).emit("END_GAME", { winners: room.winners, roomId });
            }
        }
    }

    newGame(roomId) {
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {
            user.progress = 0;
            user.ready = false;
        })

        room.usersReady = 0;
        room.winners = [];
        this.rooms.set(roomId, room);
        this.io.to(roomId).emit("UPDATE_ROOM", room);
    }
}