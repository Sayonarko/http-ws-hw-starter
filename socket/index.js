import { filterRooms } from "./helpers";
import Room from "./services/room.service";
import Game from "./services/game.service";

const uniqueUsers = new Set();
const rooms = new Map();

export default io => {
  io.on("connection", socket => {

    const username = socket.handshake.query.username;
    const roomService = new Room(rooms, io, socket);
    const gameService = new Game(rooms, io, socket);

    uniqueUsers.has(username)
      ? socket.emit("USER_EXIST")
      : uniqueUsers.add(username);

    socket.on("disconnecting", () => socket.rooms[1] && roomService.leaveRoom(socket.rooms[1]));
    socket.on("disconnect", () => uniqueUsers.delete(username));
    socket.on("CREATE_ROOM", name => roomService.createRoom(name));
    socket.on("JOIN_ROOM", roomId => roomService.joinRoom(roomId));
    socket.on("LEAVE_ROOM", roomId => roomService.leaveRoom(roomId));
    socket.on("READY_GAME", roomId => gameService.readyGame(roomId));
    socket.on("UPDATE_PROGRESS", payload => gameService.updateProgress(payload));
    socket.on("NEW_GAME", roomId => gameService.newGame(roomId));
    socket.emit("UPDATE_ROOMS", filterRooms(rooms));
  });
};