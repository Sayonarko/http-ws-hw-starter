import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import text from "../data";

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })

  .get("/texts/:id", (req, res) => {
    const id = req.params.id;
    const body = text.texts[id];
    res.json(body);
  })

export default router;
